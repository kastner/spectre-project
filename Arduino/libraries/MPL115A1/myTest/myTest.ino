#include <MPL115A1.h>
#include <SPI.h>


//MPL115A1 mpl;
void setup(){
  Serial.begin(9600);
  MPL.begin();
}

void loop() {
  float pres = MPL.pressure();
  float alt = MPL.altitude(pres);
  Serial.print("Preassure in hPa: ");
  Serial.println(pres);
  Serial.println("Altitude in meters: ");
  Serial.println(alt);
  delay(1000);
}
