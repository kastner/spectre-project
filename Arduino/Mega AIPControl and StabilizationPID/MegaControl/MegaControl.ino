/*
AIPControl_and_StabilizationPIDv4 - MEGA
This program is to be used for control and stabilization of the Aerial Imaging Platform.
Version 4 uses quaternion based PID controllers for roll, pitch and yaw stabilization.
Based on AIPControl_and_StabilizationPIDv3 by Michael Carlson (mic2169853@maricopa.edu) 
and includes code posted to the Arduino forums by Jon Russell, used by permission.
by Team Norway - Kamilla Bolstad, Alexander Fosseidbraaten, Thomas Dagsvik 
and Frederik de Lichtenberg (frederikgdl@gmail.com)
05/29/2014
*/

//Constants
//#define DEBUG

#define STATE_ZERO         0
#define STATE_S            1
#define STATE_SN           2
#define STATE_SNP          3
#define STATE_PT           4
#define STATE_READ_DATA    5
#define STATE_CHK1         6
#define STATE_CHK0         7
#define STATE_DONE         8 

#define BAUD 57600
#define UM6_GET_DATA       0xAE
#define UM6_REG_EULER_ROLL_PITCH  0x62
#define UM6_REG_EULER_YAW  0x63
#define UM6_QUAT_AB 0x64
#define UM6_QUAT_CD 0x65
#define UM6_EULER_SCALAR  0.0109863
#define UM6_QUAT_SCALAR 0.0000335693

#define PT_HAS_DATA  0b10000000
#define PT_IS_BATCH  0b01000000
#define PT_COMM_FAIL 0b00000001

#define DATA_BUFF_LEN  16
#define PAD .125
#define YAW_FLAT 1530
#define YAW_PROP_CONSTANT 5
#define ROLL_PROP_CONSTANT 4
#define ROLL_FLAT 1538
#define ZERO_ROLL 0.0
#define PITCH_PROP_CONSTANT 8
#define PITCH_FLAT 1535
#define ZG_COM_PCKT_SIZE 7
#define FILTER_VALUE 50
#define BUMP 5.0
#define BUMP1 1.0
#define KP_PITCH 8.75
#define KI_PITCH 0.00375
#define KD_PITCH 4.25
#define KP_ROLL 8.625
#define KI_ROLL 0.0015
#define KD_ROLL 3.875
#define KP_YAW 8.0
#define KI_YAW 0.00075
#define KD_YAW 4.25
#define MIN_SUM_YAW -200
#define MIN_SUM_PITCH -200
#define MIN_SUM_ROLL -100
#define INT_WIND_UP 199
#define INT_WIND_UP_ROLL 99

#define KP_X 494.176 // roll, x-axis
#define KP_Y 501.338 // pitch, y-axis
#define KP_Z 458.366 // yaw, z-axis
#define R_FIVE_DEG 0.9990482216 // real part of five degree rotation quaternion
#define I_FIVE_DEG 0.0436193874 // imaginary part of five degree rotation quaternion
#define R_ONE_DEG 0.19980964432 // real part of one degree rotation quaternion
#define I_ONE_DEG 0.00872387748 // imaginary part of one degree rotation quaternion

//includes
#include <Servo.h>
#include <quaternion.h>

//Global variable declarations
boolean activateFilter;
boolean activateStabilize;
char command;
byte incoming;
int nState = 0;
byte aPacketData[DATA_BUFF_LEN];
int n = 0;
byte c = 0;
int nDataByteCount = 0;
float pitchCenter;
//float newPitch;

Servo yawServo;
Servo rollServo;
Servo pitchServo;
float yawCenter;
int yawBump;
boolean notyawCentered;
int rollCtrl;
boolean doZeroGyros;
float mappedPitchCenter;
float mapActualPitch;
float pvPitch;
float spPitch;
float diffPitch;
float sumPitch;
float deltaDiffPitch;
float oldDiffPitch;
float pvRoll;
float spRoll;
float diffRoll;
float sumRoll;
float deltaDiffRoll;
float oldDiffRoll;
float diffYaw;
float sumYaw;
float deltaDiffYaw;
float oldDiffYaw;
int lastTime;
int deltaT;
float yaw;
float roll;
float pitch;
boolean locSet;
Quaternion <float> curQ;
Quaternion <float> desiredQ;
float quatDiffYaw;
float quatDiffPitch;
float quatDiffRoll;

float b[4]; //test vector
Quaternion <float> errorQuat;
float errorQuatVals[4];
float theta_err;
float r_x;
float r_y;
float r_z;
float newYaw;
float newPitch;
Quaternion <float> quatYawBump;
Quaternion <float> pitchBump;
Quaternion <float> rollBump;
Quaternion <float> tmpQ;

void printQuaternion(Quaternion <float> q);

byte zeroGyroCommand[] = {
  0x73, 0x6E, 0x70, 0x00, 0xAC, 0x01, 0xFD};

typedef struct {
  boolean HasData;
  boolean IsBatch;
  byte BatchLength;
  boolean CommFail;
  byte Address;
  byte Checksum1;
  byte Checksum0;
  byte DataLength;
} UM6_PacketStruct;

UM6_PacketStruct UM6_Packet;

//Setup, start up Serial connections, servos and initialize variables
void setup()
{
  Serial1.begin(BAUD);
  Serial.begin(BAUD);
  yawServo.attach(12);
  rollServo.attach(10);
  pitchServo.attach(9);
  pitchServo.writeMicroseconds(PITCH_FLAT);
  rollServo.writeMicroseconds(ROLL_FLAT);
  yawServo.writeMicroseconds(YAW_FLAT);
  notyawCentered = true;
  doZeroGyros = true;
  activateFilter = false;
  activateStabilize = false;
  mappedPitchCenter = 90.0;
  newPitch = 90.0;
  pvPitch = 0.0;
  spPitch = 0.0;
  diffPitch = 0.0;
  sumPitch = 0.0;
  deltaDiffPitch = 0.0;
  oldDiffPitch = 0.0;
  pvRoll = 0.0;
  spRoll = 0.0;
  diffRoll = 0.0;
  sumRoll = 0.0;
  deltaDiffRoll = 0.0;
  oldDiffRoll = 0.0;
  diffYaw = 0.0;
  sumYaw = 0.0;
  deltaDiffYaw = 0.0;
  oldDiffYaw = 0.0;
  locSet = false;

  quatYawBump = Quaternion <float> (R_FIVE_DEG, 0, 0, I_FIVE_DEG); // 5 degrees
  pitchBump = Quaternion <float> (R_FIVE_DEG, 0, I_FIVE_DEG, 0); // 5 degrees
  rollBump = Quaternion <float> (R_FIVE_DEG, I_FIVE_DEG, 0, 0); // 5 degrees

}
//main program loop
void loop() {
  //Get incoming commands
  if(Serial.available() > 0)
  {
    incoming = Serial.read();
    command = char(incoming);
  }
  //parse commands and set flags based on commands
  //commands are: 'w' = pitch BUMPed up, 's' = pitch BUMPed down, 
  //'p'N = pitch to N degrees , where N is float between 0-180.0,
  //'f' = toggle filter, 'q' = activate stabilization
  //'z' = deactivate stabilization, 'y'N = add N degrees to current yaw 
  //'d' = bump yaw right 10 degrees, 'a' = bump yaw left 10 degrees
  
  // Letters taken by UNO and MEGA: a b c d f j k l m o p q r s t w z
  
  switch (command)
  {
  case 'd':
    desiredQ = quatYawBump*desiredQ;
    break;    
  
  case 'a':
    desiredQ = (quatYawBump.inverse())*desiredQ;
    break;  
  
  case 'y':
    yawBump = Serial.parseInt();

    if((yawBump <= 180) && (yawBump >= -180))
    {
      float bumpRads = deg_to_rad(yawBump);//Convert input degrees to radians
      changeYaw(bumpRads);//Move yaw with bumpRads radians.
    }
    break;
    
  case '2':
    desiredQ = (rollBump.inverse())*desiredQ;
    break; 
    
  case '3':
    desiredQ = rollBump*desiredQ;
    break; 
    
  case 'f':
    if(activateFilter){
      activateFilter = false;
    }
    else{
      activateFilter = true;
    }
    break;

  case 'q':
    activateStabilize = true;
    break;

  case 'z':
    activateStabilize = false;
    locSet = false;
    break;

  case 'w':
    desiredQ = pitchBump*desiredQ;
    mappedPitchCenter = mappedPitchCenter + BUMP;
    break;

  case 's':
    desiredQ = (pitchBump.inverse())*desiredQ;
    mappedPitchCenter = mappedPitchCenter - BUMP;
    break;

  case 'p':
    newPitch = Serial.parseFloat();

    if((newPitch <= 180) && (newPitch >= -180))
    {
      float bumpRads = deg_to_rad(newPitch);//Convert input degrees to radians
      changePitch(bumpRads);
    }
    break;

  case 'b':
    newYaw = Serial.parseFloat();
    newPitch = Serial.parseFloat();
    desiredQ = Quaternion <float> (cos(newYaw/2), 0, 0, sin(newYaw/2));
    tmpQ = Quaternion <float> (cos(newPitch/2), 0, sin(newPitch/2), 0);
    desiredQ = tmpQ*desiredQ;
    
  case 'x':
    Serial.print("quatYaw retMics: ");
    Serial.println(quatYawPID());
    Serial.print("quatDiffYaw: ");
    Serial.println(diffYaw);
    
    Serial.print("pitch retMics: ");
    Serial.println(quatPitchPID());
    Serial.print("diffPitch: ");
    Serial.println(diffPitch);
    
    Serial.print("roll retMics: ");
    Serial.println(quatRollPID());
    Serial.print("diffRoll: ");
    Serial.println(diffRoll);


    desiredQ.getValues(b);
    Serial.print("desiredQ: (");
    Serial.print(b[0]);
    Serial.print(", (");
    Serial.print(b[1]);
    Serial.print(", ");
    Serial.print(b[2]);
    Serial.print(", ");
    Serial.print(b[3]);
    Serial.println("))");

    curQ.getValues(b);
    Serial.print("curQ: (");
    Serial.print(b[0]);
    Serial.print(", (");
    Serial.print(b[1]);
    Serial.print(", ");
    Serial.print(b[2]);
    Serial.print(", ");
    Serial.print(b[3]);
    Serial.println("))");
    break;
    
  case 'h':
    Serial.print("curQ: ");
    printQuaternion(curQ);
    Serial.print("desiredQ: ");
    printQuaternion(desiredQ);
    Serial.print("errorQuat: ");
    printQuaternion(errorQuat);
    
  }
  command = 0;
  //the IMU gyros tend to drift during the first 5 minutes since powerup, so 
  //send commands to zero gyros during the first 6 minutes
/*  if(doZeroGyros && millis() > 370000)
  {
    doZeroGyros = false;
    #ifdef DEBUG
    Serial.println("YAW_FLATping Zero Gyros commands");
    #endif
  }
  if(doZeroGyros && timeToZeroGyros())
  {
    sendReceiveZeroGyroCommand();
    #ifdef DEBUG
    //!!!!!debug comment out of final program
    Serial.println("Sending Zero Gyro Command");
    #endif
  }
*/

  //get data packet from IMU
  n = Serial1.available();
  if (n > 0){
    c = Serial1.read();
    //Serial.print("c=");
    //Serial.println(c, HEX);
    switch(nState){
    case STATE_ZERO : // Begin. Look for 's'.
      // Start of new packet...
      Reset();
      if (c == 's'){
        nState = STATE_S;
      } 
      else {
        nState = STATE_ZERO;
      }
      break;
    case STATE_S : // Have 's'. Look for 'n'.
      if (c == 'n'){
        nState = STATE_SN; 
      } 
      else {
        nState = STATE_ZERO;
      }
      break;
    case STATE_SN : // Have 'sn'. Look for 'p'.
      if (c == 'p'){
        nState = STATE_SNP; 
      } 
      else {
        nState = STATE_ZERO;
      }
      break;
    case STATE_SNP : // Have 'snp'. Read PacketType and calculate DataLength.
      UM6_Packet.HasData = 1 && (c & PT_HAS_DATA);
      UM6_Packet.IsBatch = 1 && (c & PT_IS_BATCH);
      UM6_Packet.BatchLength = ((c >> 2) & 0b00001111);
      UM6_Packet.CommFail = 1 && (c & PT_COMM_FAIL);
      nState = STATE_PT;
      if (UM6_Packet.IsBatch){
        UM6_Packet.DataLength = UM6_Packet.BatchLength * 4;
        //  Serial.print("Is Batch and batch length is ");
        //   Serial.println(UM6_Packet.BatchLength, DEC);
      } 
      else {
        UM6_Packet.DataLength = 4;
        //  Serial.println("Is not a batch");
      }
      break;
    case STATE_PT : // Have PacketType. Read Address.
      UM6_Packet.Address = c;
      //Serial.print(c, HEX);
      //Serial.print("\n");
      nDataByteCount = 0;
      nState = STATE_READ_DATA; 
      break;
    case STATE_READ_DATA : // Read Data. (UM6_PT.BatchLength * 4) bytes.
      aPacketData[nDataByteCount] = c;
      nDataByteCount++;
      if (nDataByteCount >= UM6_Packet.DataLength){
        nState = STATE_CHK1;
      }
      break;
    case STATE_CHK1 : // Read Checksum 1
      UM6_Packet.Checksum1 = c;
      nState = STATE_CHK0;
      break;
    case STATE_CHK0 : // Read Checksum 0
      UM6_Packet.Checksum0 = c;
      nState = STATE_DONE;
      break;
    case STATE_DONE : // Entire packet consumed. Process packet
      ProcessPacket();
      nState = STATE_ZERO;
      break;
    }//end switch
  }//end if(n>0) ie if Serial1.available
  //!!!!!debug comment out of final program
  else //we didn't get a valid packet
  {
    #ifdef DEBUG
    //Serial.println("Data not available");
    #endif
  } 
}//end loop()

void printQuaternion(Quaternion <float> q)
{
  float quatVals[4];
  q.getValues(quatVals);
  Serial.print("Q: (");
  Serial.print(quatVals[0]);
  Serial.print(" (");
  Serial.print(quatVals[1]);
  Serial.print(", ");
  Serial.print(quatVals[2]);
  Serial.print(", ");
  Serial.print(quatVals[3]);
  Serial.println("))");
}

//Helper functions

//ProcessPacket() code to extract data from the IMU

void ProcessPacket() {
  short regData = 0;
  float quat_vec_a = 0;
  float quat_vec_b = 0;
  float quat_vec_c = 0;
  float quat_vec_d = 0;

  //if we have data from quaternion registers, read it
  if (UM6_Packet.Address == UM6_QUAT_AB) 
  {
    if (UM6_Packet.HasData && !UM6_Packet.CommFail){
      regData = (aPacketData[0] << 8) | aPacketData[1];
      quat_vec_a = float(regData) * UM6_QUAT_SCALAR;
      regData = (aPacketData[2] << 8) | aPacketData[3];
      quat_vec_b = float(regData) * UM6_QUAT_SCALAR;
      if (UM6_Packet.DataLength > 4){
        regData = (aPacketData[4] << 8) | aPacketData[5];
        quat_vec_c = float(regData) * UM6_QUAT_SCALAR;
        regData = (aPacketData[6] << 8) | aPacketData[7];
        quat_vec_d = regData * UM6_QUAT_SCALAR;
      }
      curQ = Quaternion <float> (quat_vec_a, quat_vec_b, quat_vec_c, quat_vec_d);
    }
    
    if(activateStabilize)
    {
      /*
      int currTime = millis();
      deltaT = currTime - lastTime;
      lastTime = currTime;
      */

      if (!locSet) 
      {
        desiredQ = curQ;
        locSet = true;
      } 

      // calculate error quaternion
      errorQuat = (desiredQ*curQ.inverse());

      if (errorQuat.norm() < 0.99 || errorQuat.norm() > 1.01) {
        Serial.print("errorQuat.norm = ");
        Serial.println(errorQuat.norm());
        errorQuat.UnitQuaternion();
      }
      
      errorQuat.getValues(errorQuatVals);
      //(desiredQ*curQ.inverse()).getValues(errorQuatVals);
      if (errorQuatVals[0] <= 0.99999) // avoid dividing by zero
      {
        // theta_err is error angle round the axis given by r_x, r_y, r_z
        theta_err = 2*acos(errorQuatVals[0]);
        //Serial.println(theta_err);
        r_x = errorQuatVals[1]/sin(theta_err/2.0);
        //Serial.println(r_x);
        r_y = errorQuatVals[2]/sin(theta_err/2.0);
        r_z = errorQuatVals[3]/sin(theta_err/2.0);

        //printQuaternion(errorQuat);

        //yaw stabilization
        yawServo.writeMicroseconds(quatYawPID());
         
        //roll stabilization 
        rollServo.writeMicroseconds(quatRollPID());

        //pitch stabilization      
        pitchServo.writeMicroseconds(quatPitchPID());
      }
       
/*
      Serial.print("curQ: ");
      printQuaternion(curQ);
      Serial.print("desiredQ: ");
      printQuaternion(desiredQ);
      //printQuaternion(errorQuat);
      Serial.print("errorQuat: (");
      Serial.print(errorQuatVals[0]);
      Serial.print(" (");
      Serial.print(errorQuatVals[1]);
      Serial.print(", ");
      Serial.print(errorQuatVals[2]);
      Serial.print(", ");
      Serial.print(errorQuatVals[3]);
      Serial.println("))");
     */


      /*
      //yaw stabilization
      yawServo.writeMicroseconds(quatYawPID());
       
      //roll stabilization 
      rollServo.writeMicroseconds(quatRollPID());

      //pitch stabilization      
      pitchServo.writeMicroseconds(quatPitchPID());
      */
    }//end if(activateStabilize)

  }
  else
  {
    #ifdef DEBUG
    Serial.println("Unknown data");
    #endif
  }
}
//Reset() re-initializes packet struct
void Reset(){
  UM6_Packet.HasData = false;
  UM6_Packet.IsBatch = false;
  UM6_Packet.BatchLength = 0;
  UM6_Packet.CommFail = false;
  UM6_Packet.Address = 0;
  UM6_Packet.Checksum1 = 0;
  UM6_Packet.Checksum0 = 0;
  UM6_Packet.DataLength = 0;
}

// simple quaternion-based P-controller
int quatYawPID()
{
  int retMics;
  // z-axis points towards ground, has to multiply by -1
  retMics = YAW_FLAT + (KP_Z * theta_err * (-1) * r_z);
  return retMics;
}

// simple quaternion-based P-controller
int quatRollPID()
{
  int retMics;
  retMics = ROLL_FLAT + (KP_X * theta_err * r_x);
  return retMics;
}

// simple quaternion-based P-controller
int quatPitchPID()
{
  int retMics;
  retMics = PITCH_FLAT + (KP_Y * theta_err * r_y);
  return retMics;
}

float deg_to_rad(int deg)
{
  return deg * M_PI/180;
}

// applies change to yaw given by diff
// diff in radians
void changeYaw(float diff)
{
  if (diff > 0 && diff <= M_PI)
  {
    Quaternion <float> rot(cos(diff/2), 0, 0, sin(diff/2));
    desiredQ = rot*desiredQ;
  } 
  else if (diff < 0 && diff >= -M_PI)
  {
    Quaternion <float> rot(cos(-1*diff/2), 0, 0, -sin(-1*diff/2));
    desiredQ = rot*desiredQ;
  }
}

void changePitch(float diff)
{
  if (diff > 0 && diff <= M_PI)
  {
    Quaternion <float> rot(cos(diff/2), 0, sin(diff/2), 0);
    desiredQ = rot*desiredQ;
  } 
  else if (diff < 0 && diff >= -M_PI)
  {
    Quaternion <float> rot(cos(-1*diff/2), 0, -sin(-1*diff/2), 0);
    desiredQ = rot*desiredQ;
  }
}

void changeRoll(float diff)
{
  if (diff > 0 && diff <= M_PI)
  {
    Quaternion <float> rot(cos(diff/2), sin(diff/2), 0, 0);
    desiredQ = rot*desiredQ;
  } 
  else if (diff < 0 && diff >= -M_PI)
  {
    Quaternion <float> rot(cos(-1*diff/2), -sin(-1*diff/2), 0, 0);
    desiredQ = rot*desiredQ;
  }
}

//sendReceiveZeroGyroCommand sends command to zero gyros
void sendReceiveZeroGyroCommand()
{
  for(int i = 0; i < ZG_COM_PCKT_SIZE; i++)
  {
    Serial1.write(zeroGyroCommand[i]);
  }
}

//timeToZeroGyros timer for sending zero gyros command
boolean timeToZeroGyros()
{
  if((millis() < 360100) && ((millis() % 90000) < 20))
  {
    return true;
  }
  else
  {
    return false;
  }
}
