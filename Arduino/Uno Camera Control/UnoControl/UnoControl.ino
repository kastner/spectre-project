/* cameraControlv4 - SPECTRE - UNO
 This program controls the camera for the aerial imaging platform,
 gets a char over the serial connection and determines which command is sent, 
 takes pictures, sets multishoot mode, sets autofocus and
 sends reset signal to the Mega, if needed. 
 By Michael Carlson (mic2169853@maricopa.edu) 
 08/07/2013
 
 11/13/2013
 */
//Definitions: 
#define CAMERA 5
#define MEGA 4
#define AUTOFOCUS 3
//#define BAUD 9600
#define BAUD 57600
#define PI 3.14159265358979323846
#define yawDiff 30 //Panorama: How many degrees to move inbetween each picture. Dependent on lense-width and desired picture overlap.
//#define DEBUG

//Libraries:
#include <GPS_UBLOX.h> // GPS library
#include "SoftwareSerial.h"
#include <stdio.h>
#include <string.h>
#include <math.h>       /* fmod */
#include <MPL115A1.h> // barometer library
#include <SPI.h>
#include <Streaming.h>


//Communication Declarations
SoftwareSerial arduinoS(2,8);//rx/tx
int i;
char MEGAReader[26]; //current config: [h1,h2,h3,x1,x2,x3,y1,y2,y3,z1,z2,z3] where x123 are 000-360 degrees in x axis
//char serialReader[12]; // [type,latN,lat1,lat2,lat3,lat4,lonN,lon1,lon2,lon3,lon4,lon5]
char command;

//Camera Declarations
boolean multishoot, autoFocus;
long lastPicTime, currTime, waitTime, inputTime;
int yawBump;

//GPS Declarations
float HDG = 0.0, nHDG; // current heading and new heading
float lat2, lon2;//target lattitude and longitude
float lat1, lon1; //Spectre lattitude and longitude
float dlat, dlon, dist; // delta lattitude, delta longitude, distance between points
float tc1 = 0.0; // new hdg (maybe remove this later)

//Panoram Declarations:
int panoramaWidth, inputWidth, turns;
String moveString;

//Barometer Declarations:
float pres, alt, AOA, elevationT;

//setup: set output pins, start serial connection and initialize variables
void setup()
{
  pinMode(CAMERA, OUTPUT);
  pinMode(MEGA, OUTPUT);
  pinMode(AUTOFOCUS, OUTPUT);

  Serial.begin(BAUD);
  arduinoS.begin(57600);

  multishoot = false;
  lastPicTime = 0;
  waitTime = 10;
  
  #ifdef DEBUG
  Serial.println("Welcome to the Spectre Control system...");
  Serial.println("To pilot write commands: \n't' = take picture, \n'm' = toggle multishoot mode, \n'm'N = toggle ON with N seconds waittime between pics if OFF, \n'r'=reset the MEGA \n'c' = turn on autofocuss, \n'k' = getGPSloc gets gps location and altitude, \n'b' = gets new heading and AOA.\n\n");
  Serial.println("GPS UBLOX library test");
  #endif
  
  //Panorama inits, to be sent to MEGA:
  //moveString = String("y ");
  panoramaWidth = 180;//Default value

  GPS.Init();   // GPS Initialization
  MPL.begin(); //inits the Barometer

  delay(1000); //needed to settle

}//end setup()

void loop()
{
  //take readings
  GPS.Read();
  pres = MPL.pressure();
  alt = MPL.altitude(pres);
  lat1 = GPS.Lattitude/10000000;
  lon1 = GPS.Longitude/10000000;

  
  if(Serial.available() > 0) // if input from the PC
  {
    command = Serial.read();
    // commands: 
    //'t' = take picture, 'm' = toggle multishoot ON if OFF, 
    //'m'N = toggle ON with N seconds waittime between pics if OFF, 
    //'r'= resets the MEGA  'c' = turn on autofocuss, 
    //'k' = getGPSloc gets gps location and altitude, 'b' = gets new heading and AOA."
    //'o' W = Activate panorama with W degrees width of final panorama.

    // Any other inputs are sent to the MEGA arduino.

    // Letters taken by UNO and MEGA: a b c d f j k l m o p q r s t w z

    switch(command)
    {
    case '?':
      Serial.println("Commands: 'wasd' = direct camera control (5 degree increments),\n 't' = take picture, 'm' = toggle multishoot mode \n, 'm'N = toggle ON with N seconds waittime between pics if OFF,\n 'r'=reset the MEGA \n 'c' = turn on autofocuss,\n 'k' = getGPSloc gets gps location and altitude,\n 'o'N = panorama with input N.,\nb' = gets new heading and AOA.\n");
      break;

      //Handled by UNO
    case 't':
      takePicture();
      break;

    case 'm':
      if(multishoot){ //Switch timelapse off
        multishoot = false;
        //digitalWrite(AUTOFOCUS, LOW); //Deactivate autofocus
      }

      else{ //Switch timelapse on
        multishoot = true;
        inputTime = Serial.parseInt(); //Get the wait time inbetween each picture.
        if(inputTime>0){
          waitTime=inputTime;
        }
        Serial << "In multihsoot with wait time: " << waitTime << " seconds. \n\n";
        waitTime = waitTime*1000l;//convert to milliseconds.
        //digitalWrite(AUTOFOCUS, HIGH); //Activate autofocus
      }
      break;

    case 'o': //panorama
      inputWidth = Serial.parseInt();
      
      if(inputWidth>=0 && inputWidth<=360){
        panoramaWidth = inputWidth;
      }
      
      delay(250);
      createPanorama();//panoramaWidt is defaulted to 180 degrees if the input is ridiculous or non-existing.
      break;

    case 'r':
      Serial.println("RESET");
      digitalWrite(MEGA, LOW);
      delay(250);
      digitalWrite(MEGA, HIGH);
      break;

    case 'c':
      if (autoFocus) {
        digitalWrite(AUTOFOCUS, LOW);
        autoFocus = false;
      }
      else {
        digitalWrite(AUTOFOCUS, HIGH);
        autoFocus = true;
      }
      break;

    case 'k': // prints relevant sensor data.
      Serial.print("GPSFix:"); // when this is 1 we have a gps lock, else the gps data may be invalid.
      Serial.println((int)GPS.Fix);
      Serial.print("Lattitude: ");
      Serial.println(lat1);
      Serial.print("Longitude: ");
      Serial.println(lon1);
      Serial.print("Pressure: ");
      Serial.println(pres);
      Serial.print("Altitude: ");
      Serial.println(alt);
      break;

    case 'b':
      //Serial.println("Insert target lattitude in decimals (- for south), target longitude in decimals (- for west), and target elevation");
      lat2 = Serial.parseFloat();
      Serial.print("Target lattitude: ");
      Serial.println(lat2);

      lon2 = Serial.parseFloat();
      Serial.print("Target longitude: ");
      Serial.println(lon2);

      elevationT = Serial.parseFloat();
      Serial.println("Target elevation: ");
      Serial.println(elevationT);

      Serial.print("Calculating....\n");
      calcNewAngles();
      Serial.print("finishCalculations... \n");
      Serial << "Sending to Mega (nHDG, AOA): " << nHDG << " " << AOA << "\n";
      arduinoS <<"b" << nHDG << " " << AOA;
      break;

    default:
      arduinoS.print(command);
      Serial.print("Default: ");
      Serial.println(command);
      break;

    }//end switch(command)
  }//end if(Serial.available() > 0)

  //set command to null to keep the command from repeating
  command = NULL;

  //get the time for multishoot mode
  currTime = millis();

  //if multishoot is enabled
  if(multishoot){
    if( (currTime - lastPicTime) >= waitTime){
      #ifdef DEBUG
      Serial << "Before TC, lastPicTime is " << lastPicTime << '\n';
      #endif
 
      takePicture();

      lastPicTime = currTime;

      #ifdef DEBUG
      Serial << "After taking pic, lastPicTime is " << lastPicTime << "\n\n";
      #endif
    }
  }//end if(multishoot)
}//end loop

//////////////
//FUNCTIONS://
//////////////

void createPanorama(){
  turns = panoramaWidth/yawDiff;
  /*
  moveString += yawDiff;
  */
  
  #ifdef DEBUG
  Serial << "Moving " << yawDiff << " degrees " << turns << " times,\n Panorama width: " << panoramaWidth << "\n\n";
  #endif
  delay(1000);
  
  for(int i=0;i<turns;i++){
    takePicture();

    #ifdef DEBUG
    Serial << "Picture nr. " << i+1 << " taken.\n\n";
    #endif
    delay(3500);
    
    //arduinoS << moveString;
    if(i<turns-1){//Temporary solution while we figure out the moveString
      for(int j=0;j<(yawDiff/5);j++){
        arduinoS << 'd';}
    }
    
    /*
    #ifdef DEBUG
    Serial << "Sending moveString, (" << moveString << "), to MEGA \n";
    #endif
    */

    delay(1000);
  }
  
  #ifdef DEBUG
  Serial << "All done, with " << turns << " pictures taken.\n\n";
  #endif
}//end createPanorama()




//void takePicture() sends signal to camera to take a picture
void takePicture() 
{
  #ifdef DEBUG
  Serial.println("in takePicture()");
  #endif
  //delay(500);
  digitalWrite(CAMERA, HIGH);
  delay(250);//delayMicroseconds(1000);
  digitalWrite(CAMERA, LOW);
} //end takePicture()





//void calcNewAngles gives new heading and AOA for the Spectre
void calcNewAngles(){
  //Notes:
  // When doing coordinates in degrees, minutes and seconds, North/South comes before West/East -> (NS , EW).
  //Longitude is EastWest, Lattitude is NorthSouth. Think that the equator is Long.

  dlat = lat2 - lat1; //difference in lattitude.
  dlon = (lon2 - lon1)*cos(lat1); //difference in longitude
  dist = 111.32*(sqrt(dlon*dlon + dlat*dlat));

  float h = alt - elevationT;
  AOA = atan(dist/h); //angle from straight down = 0 rad.

  //Target in North Hemisphere
  if (dlat > 0)
  {
    if (dlon != 0) tc1 = atan(dlon/dlat); //EastWest
    else tc1 = 0; //Straight North
  }//end North

  //Target in South Hemisphere:
  if (dlat < 0)
  {
    if (dlon > 0) //East
      tc1 = (PI/2) - atan(dlat/dlon);
    if (dlon < 0) //West
      tc1 = -(PI/2) - atan(dlat/dlon);
    if (dlon == 0) //Straight South
      tc1 = PI;
  }//end South

  //EASTWEST
  if (dlat == 0)
  {
    if (dlon > 0) //East 
      tc1 = (PI/2); //no rotation
    if (dlon < 0) //West
      tc1 = -(PI/2);
    if (dlon == 0) // right below us
      tc1 = 0; //no rotation
  }//end NEASTWEST

  nHDG = tc1-HDG;

  //For debug:
  Serial.print("old HDG: ");
  Serial.println(HDG);
  Serial.print("tc1: ");
  Serial.println(tc1);
  Serial.print("nHDG: ");
  Serial.println(nHDG);
  Serial.print("AOA for target: ");
  Serial.println(AOA);

}//end calcNewHDG


