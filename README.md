# A software update to the Spectre
Welcome to Spectre, a software update for the Stabilized Aerial Camera Platform. The system consists of two main classes in C and 4 libraries written in C++. 

## Overview of system

### Main Classes

**UnoControl.ino:** This program runs on the Arduino Uno of the system. It gets a char over the serial connection and determines which command is to be executed. Commands not recognized by the Uno are sent to the Mega. The program takes pictures, sets multishoot mode (timelapse), sets autofocus, controls panorama picture functionality, calculates angles for autonomous GPS targeting and sends reset signal to the Mega, if needed. It also acts as the master of the Mega, GPS and barometer.

**MegaControl.ino:** The Mega handles stabilization and motor control. In this software update this is handled trough the use of quarternions to avoid gimbal lock.

###Libraries

**MPL115A1.cpp and MPL115A1.h:** This is the library for the barometer. The current implementation reads out pressure and calculates the camera platform's altitude above sea level in meters. It is possible to implement temperature readings, but this functionality is not present in the current system.

**GPS_UBLOX.cpp and GPS_UBLOX.h:** This is the GPS library. It handles setup and reading of GPS data.

**Quarternion Library:** Library for quaternion operations and conversions to euler angles.

**Streaming.h:** A neat way to concatenate strings upon printing.

##Uploading software to the Spectre

To upload new software you need to download Arduino IDE:
<http://arduino.cc/en/main/software>

**For Uno**  
Connect your computer to the Uno with a USB cable. Ensure that the cables in the RX, TX pins are pulled out. Open the UnoControl.ino. When uploading the code to the Uno make sure the Streaming.h, MPL115A1 and GPS_UBLOX libraries are included by pushin Sketch -> Import Library -> Add Library. Add the folder that encloses the library. Make sure you have selected the right type of board by following Tools -> Board -> “Arduino Uno”. Lastly, select the appropriate serial port by following Tools -> Serial Port -> .../usb[...].tty. Verify the code, and press upload.

**For Mega**  
Connect your computer to the Mega with a USB cable. Ensure that the cables in the RX, TX and Reset pins are pulled out. Open the MegaControl.ino. When uploading the code to the Mega make sure the quaternion.h library is included by following Sketch -> Import Library -> Add Library. Add the folder that encloses the library. Make sure you have selected the right type of board by following Tools -> “Arduino Mega 560 or Mega ADK”. Lastly, select the appropriate serial port by following Tools -> Serial Port -> .../usb[...].tty. Verify the code, and press upload.

**To upload previos code**  
	The IMU needs to be set to euler angles. To do this download and install the “CH Robotics Serial Interface” from <http://www.chrobotics.com/shop/orientation-sensor-um6>. When open and the IMU is connected to the pc, select the “Configuration” tab, and read the data. Under “Communication register” set quaternions to 0 and euler angles to 1. Then select “RAM commit”, and when finished press “FLASH commit”.
	The wires between the Arduinos must be removed. The two wires from the radio needs to be connected to the Mega. The white wire should be connected to both the Mega and the Uno, the blue should only be connected to the Mega.
	Upload old code and have fun.

**General**  
When programming without the rig present, the code can be compiled to check or errors by clicking verify.

Calibrate the IMU before running the code, please refer to the Wiki to see how this is done. 

When the code is uploaded correctly on both Arduino boards you can run the system from your computer by connecting to the system's serial radio.

##Commands

Commands are typed into a terminal window attached to the COM port to which the Serial radio is attached.

‘f' = Start simple filter to filter out any outlier data  
'q' = Start stabilization  
'2' = Roll left 5 degrees  
'3' = Roll right 5 degrees  
'w' = Pitch up 5 degrees  
'a' = Yaw left 10 degrees  
's' = Pitch down 5 degrees  
'd' = Yaw right 10 degrees  
'r' = Reset the Mega Arduino (send commands to restart filter and stabilization after reset)  
'c' = Enable auto focus  
'j' = Disable auto focus (Used when changing the focal length of an image, then re-enable)  
't' = Take a picture  
'm' N = Toggle multishoot mode. When toggling on, include N = seconds inbetween each picture  
'b' N M X = autonomous targeting on target with decimal cordinates N M and altitude X meters over sea level (pres 0 to assume zero distance in height between the launch point of the SACP and the target)  
'k' = get reading of GPS and barometer  
'o' W = Takes pictures for a panorama W degrees wide  

##The systems main components and their functionalities

###UnoControl.ino   
Program running on Arduino Uno.  
Baud: 57600  
SoftwareSerial to Mega: arduinoS(2,8) Baud 57600  

**Methods**  
setup(): initializes Uno, initializes GPS and Barometer (MPL)  
	
loop(): reads from GPS and barometer, takes in a char and react on the command (see commands)
	
createPanorama(): Takes a picture, turn 30 degrees, takes another picture –> continues until N/30 pictures are taken, where N is the width of the panorama, given as input upon calling the command ‘o’. For every iteration, it sends the command ‘d’ five times to the Mega, so as to make the rig turn 30 degrees horizontally.
	
takePiture(): takes picture.
	
calcNewAngles(): computes new heading(yaw) and angle of attack(pitch) in radians. Values from this function is sent to the Mega for servo control in case 'b' (autonomous GPS targeting).

###MegaControl.ino
Program running on Arduino Mega.  
	Baud: 57600  

**Methods**  
setup(): initalizes Mega

loop(): reads data from IMU and commands from Uno. Stabilizes

###MPL115A1 library
**Methods**  
begin(): sets up the barometer, call this befor any attemts of reading.   
suthdown(): puts the barometer to sleep.  
pressure(): Reads out current altitude in Pa (kPa accuracy), based on instructions from data sheet, returns it as a float.  
altitude(float pressure): Calculates altitude in meters above sea level based on current pressure reading.  

Global instance of object MPL

For more info about the MPL115A1:
<http://www.freescale.com/files/sensors/doc/data_sheet/MPL115A1.pdf>

###GPS_UBLOX library
**GPS configuration**  
Ublox protocol  
Baud rate : 38400  
Active messages :   
		NAV - POSLLH Geodetic Position Solution, PAGE 66 of datasheet  
		NAV - VELNED Velocity Solution in NED, PAGE 71 of datasheet  
		NAV - STATUS Receiver Navigation Status  
			or   
		NAV - SOL Navigation Solution Information

**Methods** 
Init(): GPS Initialization  
Read(): Call this funcion as often as you want to ensure you read the incomming gps data  
		
**Properties**  
Lattitude : Lattitude * 10,000,000 (long value)  
Longitude : Longitude * 10,000,000 (long value)  
Altitude :	Altitude * 100 (meters) (long value)  
Ground_speed : Speed (m / s) * 100 (long value)  
Ground_course : Course (degrees) * 100 (long value)  
NewData : 1 when a new data is received.  
You need to write a 0 to NewData when you read the data  
Fix : 1: GPS FIX, 0: No Fix (normal logic)
			

###Quarternion Library
Documentation in quaternion.h  

Includes definitions for ‘=’, ‘+’, ‘-’, ‘*’, ‘/’ ‘+=’, ‘-=’, ‘*=’, ‘/=’, ‘!=’ and ‘==’ for quaternions.

**Notable functions include:**  
inverse(): inverts the quaternion  
UnitQuaternion(): scales the quaternion into a unit quaternion, norm equals 1  
toEulerCHR(): returns a conversion to euler angles using formulas specified on <http://www.chrobotics.com/library/understanding-quaternions>. Note: conversion to euler angles brings back gimbal lock.  


###Streaming Library
Let’s define two numbers:  
Float e = 2.71828  
Float pi = 3.1415  
In mainstream programming languages you usually are allowed to concatenate strings and numbers in one way or another upon printing to the console. Say you wanted to print Two cool numbers are e, 2.71828, and pi, 3.1415. This is really tedious when writing code for Arduino.   You’d have to write:  

	Serial.print(“Two cool numbers are e, “);
	Serial.print(e);
	Serial.print(“, and pi, “);
	Serial.print(pi);
	
Very repetative, as you can see. With the streaming library, however, you are allowed to print it like this:

	Serial << “Two cool numbers are e, “ << e << “, and pi, “ << pi;
	
The << works the same way the + usually does, and whatever you write in the beginning is where the concatenated string will be dumped. It works with any class that derives from Print. Instead of Serial, you could for instance write to another Arduino board’s serial. In that case you would instantiate the other board with a name, f.eks. arduinoS, in which case the sentence would be:

	arduinoS << “Two cool numbers are e, “ << e << “, and pi, “ << pi;
	
To read more about it, visit <http://arduiniana.org/libraries/streaming/>.